//todo: add fix for seg fault on large numbers

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>


int isprime(int num, int divisor);
void itos(int i, char *str);

int main(int argc, char** argv)
//find if number is a prime
{
	int false, time;
	int num;
	char nstring[100];

	if( argc == 2){
		printf("ok lets go... \n");

		num = atoi(argv[1]); //convert input string to num
		itos(num, nstring);  //check no error from large str
		if (strcmp(nstring, argv[1]) != 0){
			printf("num2big\n");
			return 0;
		}

		if(num == 1 || num == 0){
			printf("not prime...\n"); //special case 
			return 0;                 // for 1 and 0
		} if(num == 2){
			printf("its prime!\n"); //special case for 2
			return 0;
		}

		false = isprime(num, 2);
		if(!false) { printf("its prime!\n"); return 0; }
		printf("not prime...\n");
	}
}

void itos(int i, char *str)
//recursion integer to string
{
	char buff[100];
	int j;
	
	for (j=0; j < 100; j++) //clear string
		buff[j] = '\0';

	if (i/10 > 0)
		itos(i/10, str);
	buff[0] = i % 10 +48;
	strcat(str, buff);
}

int isprime(int num, int divisor)
//some sort of recursion
{
	if (divisor > sqrt(num))
		return 0;
	if (isprime(num, divisor + 1))
		return 1;	
	else if (num % divisor == 0)
		return 1;
	else 
		return 0;
}
